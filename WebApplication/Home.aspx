﻿<%@ Page Language="vb" Title="Kiosk Login" MasterPageFile="~/DF_Kiosk.master" CodeBehind="Home.aspx.vb" Inherits="DFKiosk.Home" %>

<asp:Content ID="Home" ContentPlaceHolderID="MainContent" runat="server" > 
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="Customer Content">
                <asp:Label ID="Label3" runat="server" Text="Label" Visible="False"></asp:Label>
		        <br /><br />

		        <asp:Label ID="Label4" runat="server" Text="Label" Visible="False"></asp:Label>
		        <br /><br />
            </div>

            <asp:GridView ID="GridView" 
                     runat="server"
                     EnableViewState="True"
                     AutoGenerateColumns="False"
                     ShowFooter = "False"
                     OnRowCommand="GridView_RowCommand" GridLines="Horizontal" BorderStyle="None"
                     DataSourceID="SqlDataSource1" CssClass="gridview">
                    <AlternatingRowStyle CssClass="gridviewAltItem" />
                    
                <Columns>
                    <asp:TemplateField HeaderText="Quantity" HeaderStyle-HorizontalAlign="Right" 
                        ItemStyle-HorizontalAlign="Right"><ItemTemplate>
                    <asp:TextBox ID="Quantity" runat="server" Text = '<%# Bind("Quantity") %>' Font-Size="X-Large" Height="30px" Width="50px"></asp:TextBox></ItemTemplate>
                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Right"></ItemStyle></asp:TemplateField>
            
                    <asp:ButtonField runat="server" CausesValidation="false" Text="&lt;img src=Images/BuyButton2.jpg /&gt;" 
                        CommandName="Buy" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Bottom"></ItemStyle></asp:ButtonField>
                    
                    <asp:BoundField DataField="Product_Desc" HeaderText="Product Description" HeaderStyle-HorizontalAlign="Left" SortExpression="PRODUCT_DESC" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                    <asp:BoundField DataField="Id_No" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                    <asp:BoundField DataField="Item_Id_No" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                    <asp:BoundField DataField="Quantity" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                    <asp:BoundField DataField="Price_Unit" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                    <asp:BoundField DataField="Product_Id_No" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                    <asp:BoundField DataField="Unit_Meas_Qty_Cannabis" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                            
                </Columns>

                    <HeaderStyle BackColor="#FFCC00" BorderStyle="None" />
            </asp:GridView>
                    
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DFKiosk.My.MySettings.db1DFConnectionString %>"
        SelectCommand="csp_PPBILL_Kiosk_Get" SelectCommandType="StoredProcedure">
        <SelectParameters>
           <asp:QueryStringParameter QueryStringField="SRN" Name="SRNNO" Type="String" />
           
        </SelectParameters></asp:SqlDataSource>

        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DFKiosk.My.MySettings.db1DFKioskConnectionString %>" 
        InsertCommand="csp_CART_Insert" InsertCommandType="StoredProcedure"
        SelectCommand="csp_CART_Count" SelectCommandType="StoredProcedure"
        UpdateCommand="csp_CART_Update" UpdateCommandType="StoredProcedure" >
        
        <InsertParameters>
            <asp:Parameter Name="SessionId" Type="String" />
            <asp:Parameter Name="CustIdNo" Type="Int32" />
            <asp:Parameter Name="CareIdNo" Type="Int32" />
            <asp:Parameter Name="InvItemIdNo" Type="Int32" />
            <asp:Parameter Name="Quantity"/>
            <asp:Parameter Name="ProductIdNo" Type="Int32" />
            <asp:Parameter Name="ProductDesc" Type="String" />
            <asp:Parameter Name="UnitCost" />
            <asp:Parameter Name="UnitMeasQtyCannabis" />
            <asp:Parameter Name="AccountIPAddress" Type="String" />
            <asp:Parameter Name="AccountOrigin" Type="String" />
           </InsertParameters>

        <SelectParameters>
            <asp:Parameter Name="SessionId" Type="String" />
            <asp:Parameter Name="CustIdNo" Type="Int32" />
            <asp:Parameter Name="ProductIdNo" Type="String" />
        </SelectParameters>

        <UpdateParameters>
            <asp:Parameter Name="SessionId" Type="String" />
            <asp:Parameter Name="CustIdNo" Type="Int32" />
            <asp:Parameter Name="ProductIdNo" Type="String" />
            <asp:Parameter Name="Quantity" />
            <asp:Parameter Name="UnitMeasQtyCannabis" />
        </UpdateParameters>
        
    </asp:SqlDataSource>
    </ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
