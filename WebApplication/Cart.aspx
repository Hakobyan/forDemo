﻿<%@ Page Language="vb" MasterPageFile="~/DF_Kiosk.master" CodeBehind="Cart.aspx.vb" Inherits="DFKiosk.Cart" %>

<asp:Content ID="Home" ContentPlaceHolderID="MainContent" runat="server" > 
	
    <asp:LinkButton ID="btnBack" runat="server" Text="Back to Home Page" OnClick="btnBack_Click"></asp:LinkButton>
    
    <br /><br />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
		<ContentTemplate>
            <div class="Shopping Cart">
                <asp:GridView ID="GridView" 
                     runat="server"
                     EnableViewState="True"
                     AutoGenerateColumns="False"
                     ShowFooter = "True"
                     DataSourceID="SqlDataSource2" CssClass="gridview" 
                     OnRowDataBound="GridView_RowDataBound"
                     OnRowCommand="GridView_RowCommand" GridLines="Horizontal" BorderStyle="None">
                    <AlternatingRowStyle CssClass="gridviewAltItem" />
                    <FooterStyle CssClass="gridviewFooter" />

            <Columns>
                <asp:BoundField DataField="Product_Id_No" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                
                <asp:BoundField DataField="Product_Desc" HeaderText="Description" 
                    HeaderStyle-HorizontalAlign="Left" SortExpression="Product_Desc" >
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:BoundField>

                <asp:TemplateField HeaderText="Quantity" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"><ItemTemplate>
                    <asp:TextBox ID="Quantity" runat="server" Text = '<%# Bind("Quantity") %>' Font-Size="X-Large" Height="30px" Width="50px" ></asp:TextBox></ItemTemplate>
                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Right"></ItemStyle></asp:TemplateField>

                <asp:ButtonField CommandName="UpdateItem" Text="update quantity" 
                    ItemStyle-HorizontalAlign="Center" >
                            
                <ItemStyle HorizontalAlign="Center" />
                </asp:ButtonField>
                            
                <asp:BoundField DataField="Unit_Cost"
                    ItemStyle-HorizontalAlign="Right"
                    DataFormatString="{0:c}"
                    HeaderText="Unit Cost" 
                    HeaderStyle-HorizontalAlign="Right"
                    SortExpression="Unit_Cost" >
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField DataField="Total"
                    ItemStyle-HorizontalAlign="Right"
                    DataFormatString="{0:c}"
                    HeaderText="Total" 
                    HeaderStyle-HorizontalAlign="Right"
                    FooterStyle-HorizontalAlign="Right"
                    SortExpression="Total" >
                    <FooterStyle HorizontalAlign="Right" />
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>
                
                <asp:ButtonField CommandName="DeleteItem" Text="remove item" 
                    ItemStyle-HorizontalAlign="Center" >
                   <ItemStyle HorizontalAlign="Center" />
                </asp:ButtonField>

                <asp:BoundField DataField="Unit_Meas_Qty_Cannabis" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                <asp:BoundField DataField="TotalGrams" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                <asp:BoundField DataField="Id_No" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                                             
            </Columns>
            
                    <HeaderStyle BackColor="#FFCC00" />
            
        </asp:GridView>
    </div>
    
    <br />
    
    <div>
        <asp:Button ID="btnPlaceOrder" runat="server" Text="Place Order" 
                Visible="True" onclick="btnPlaceOrder_Click" class = "btn-primary" />

        <asp:Button ID="btnCancelOrder" runat="server" Text="Cancel Order" 
                Visible="True" onclick="btnCancelOrder_Click" class = "btn-primary" />
                       

    </div>
           
    <br />
    
    <asp:SqlDataSource 
        ID="SqlDataSource2" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:DFKiosk.My.MySettings.db1DFKioskConnectionString %>"
        SelectCommand="csp_CART_Select" SelectCommandType="StoredProcedure"
        DeleteCommand="csp_CART_Item_Delete" DeleteCommandType="StoredProcedure"
        UpdateCommand = "csp_CART_Quantity_Update" UpdateCommandType="StoredProcedure">
        
        <SelectParameters>
            <asp:Parameter Name="SessionId" Type="String" />
            <asp:Parameter Name="CustIdNo" Type="Int32" />
        </SelectParameters>
     
        <DeleteParameters>
            <asp:Parameter Name="IdNo" Type= "Int32" />
        </DeleteParameters>
        
        <UpdateParameters>
            <asp:Parameter Name="Quantity" Type= "Int32" />
            <asp:Parameter Name="UnitMeasQtyCannabis" />
            <asp:Parameter Name="IdNo" Type= "Int32" />
        </UpdateParameters>
        
     </asp:SqlDataSource>

     <asp:SqlDataSource 
        ID="SqlDataSource3" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:DFKiosk.My.MySettings.db1DFKioskConnectionString %>"
        DeleteCommand="csp_CART_Delete" DeleteCommandType="StoredProcedure"
        UpdateCommand = "csp_CART_OrderComplete" UpdateCommandType="StoredProcedure">
        
        <DeleteParameters>
            <asp:Parameter Name="SessionId" Type="String" />
        </DeleteParameters>
        
        <UpdateParameters>
            <asp:Parameter Name="SessionId" Type="String" />
        </UpdateParameters>
        
     </asp:SqlDataSource>
        
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
