﻿<%@ Page Language="vb" MasterPageFile="~/DF_Kiosk.master" CodeBehind="Inventory.aspx.vb" Inherits="DFKiosk.Inventory" %>

<asp:Content ID="Home" ContentPlaceHolderID="MainContent" runat="server" > 
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
		<ContentTemplate>
            <div class="columnleft">
                <asp:LinkButton ID="btnBack" runat="server" Text="Back to Home Page" OnClick="btnBack_Click"></asp:LinkButton>
            </div>
            <div class="columnrightjustified">
                <asp:TextBox ID="txtSearch" runat="server" Height="50px" Font-Size="X-Large"></asp:TextBox>
                <asp:TextBox ID="txtSearchHidden" runat="server" Visible="False"></asp:TextBox>
                <asp:Button ID="btnSearch" runat="server" Text="Search" AutoPostBack="True" OnClick="btnSearch_Click" 
                    class = "btn-primary" />
            </div>
            <div class="clear"></div>
            <br />
            <div class="Inventory Content">
                <asp:Label ID="lblStrain" runat="server" Text="Cannabis Strain: "></asp:Label>
                &nbsp;&nbsp;
                <asp:DropDownList ID="ddlStrain" runat="server" AutoPostBack="True"  Height="50px" Font-Size="X-Large">
                    <asp:ListItem Value="Default value" Text="" />
                    <asp:ListItem Value = "Indica" Text="Indica" />
                    <asp:ListItem Value = "Sativa" Text="Sativa" />
                    <asp:ListItem Value = "Hybrid" Text="Hybrid" />
                </asp:DropDownList>
               
                <asp:Label ID="lblCategory" runat="server" Text="&nbsp;&nbsp;Category: "></asp:Label>
                &nbsp;&nbsp;
                <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" Height="50px" Font-Size="X-Large"
                    DataSourceID="odsCategory" DataTextField="GROUP_DESC" DataValueField="ID_NO">
                </asp:DropDownList>
                <br /><br />
        
                <asp:ObjectDataSource ID="odsCategory" runat="server" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DFKiosk.dsDB1TableAdapters.taPPINVT_GROUP">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlStrain" Name="Strain" PropertyName="SelectedValue"
                            Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
            </div>

            <asp:Panel runat="server" ID="panel" CssClass="DialogueBackground" Visible="false"> 
                <div class="Dialogue">
                <div class="popup"> 
                    <asp:Image ID="Image1" runat="server" Height="400px" Width="400px" ImageUrl="~/Images/WebsiteLogoNew.png" />
                        <div class="clear10">
                        </div>
                    <asp:Button ID="can1" runat="server" Text="Close" BorderStyle="None" CssClass="btn" OnClick="can1_Click" />
                        <div class="clr">
                        </div>
                    </div>
                </div>
            </asp:Panel>
            
            <div class="GridView"> 
                <asp:GridView ID="GridView" runat="server" AutoGenerateColumns="False"
                DataSourceID="SqlDataSource1" CssClass="gridview"  
                OnRowCommand="GridView_RowCommand" AllowPaging="True" PageSize="5" 
                    GridLines="Horizontal" BorderStyle="None" CellSpacing="1" 
                    PagerStyle-Height="50px">
                <AlternatingRowStyle CssClass="gridviewAltItem" />
                <PagerSettings FirstPageText="First Page" LastPageText="Last Page" 
                        Position = "Top"
                        Mode="NextPreviousFirstLast" NextPageText="&amp;gt" />
                <PagerStyle BorderStyle="None" />
                      
                <Columns>

                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton" runat="server" CausesValidation="false" 
                                Height="50px" Width="50px" CommandArgument='<%# Eval("Variety_Image") %>' OnClick = "ImageButton_Click"
                                CommandName="VarietyImage" ImageUrl='<%# Eval("Variety_Image") %>' Text="Button" />
                        </ItemTemplate>
                    </asp:TemplateField>
                                          
                    <asp:TemplateField HeaderText="Quantity" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"><ItemTemplate>
                    <asp:TextBox ID="Quantity" runat="server" Text='<%# Bind("Quantity") %>' Font-Size="X-Large" Height="30px" Width="50px" ></asp:TextBox></ItemTemplate>
                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Right"></ItemStyle></asp:TemplateField>

                    <asp:ButtonField CausesValidation="false" Text="&lt;img src=Images/BuyButton2.jpg /&gt;" 
                        CommandName="Buy" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Bottom"></ItemStyle></asp:ButtonField>
                    
                    <asp:BoundField DataField="Quantity_On_Hand" HeaderText="In Stock" HeaderStyle-HorizontalAlign="Left" SortExpression="QUANTITY_ON_HAND">
                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
            
                    <asp:BoundField DataField="Price_Unit"
                        ItemStyle-HorizontalAlign="Right"
                        DataFormatString="{0:c}"
                        HeaderText="Unit Price" 
                        HeaderStyle-HorizontalAlign="Right"
                        SortExpression="PRICE_UNIT" >
                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Right"></ItemStyle></asp:BoundField>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" style="cursor:pointer"
                                NavigateUrl='<%# Eval("Variety_Website") %>' Text="More Info"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:BoundField DataField="Product_Desc" HeaderText="Description" HeaderStyle-HorizontalAlign="Left" SortExpression="PRODUCT_DESC" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                                            
                    <asp:BoundField DataField="Variety_Image" HeaderStyle-CssClass="hidden" 
                        FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" >
                    <FooterStyle CssClass="hidden" />
                    <HeaderStyle CssClass="hidden" />
                    <ItemStyle CssClass="hidden" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Id_No" HeaderStyle-CssClass="hidden" 
                        FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" >
                    <FooterStyle CssClass="hidden" />
                    <HeaderStyle CssClass="hidden" />
                    <ItemStyle CssClass="hidden" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Unit_Meas_Qty_Cannabis" 
                        HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" 
                        ItemStyle-CssClass="hidden" >
                                                                                            
                    <FooterStyle CssClass="hidden" />
                    <HeaderStyle CssClass="hidden" />
                    <ItemStyle CssClass="hidden" />
                    </asp:BoundField>
                                                                                            
                </Columns>
                    
            </asp:GridView>
        </div> 
                
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DFKiosk.My.MySettings.db1DFConnectionString %>"
        SelectCommand="csp_PPINVT_Kiosk_Get" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlStrain" Name="Strain" PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="ddlCategory" Name="GroupIdNo" PropertyName="SelectedValue" Type="INT32" />
            <asp:ControlParameter ControlID="txtSearchHidden" Name="ProductDesc" PropertyName="Text" Type="String"/>
        </SelectParameters>
    </asp:SqlDataSource>

       
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DFKiosk.My.MySettings.db1DFKioskConnectionString %>" 
        InsertCommand="csp_CART_Insert" InsertCommandType="StoredProcedure"
        SelectCommand="csp_CART_Count" SelectCommandType="StoredProcedure"
        UpdateCommand="csp_CART_Update" UpdateCommandType="StoredProcedure" >
        
        <InsertParameters>
            <asp:Parameter Name="SessionId" Type="String" />
            <asp:Parameter Name="CustIdNo" Type="Int32" />
            <asp:Parameter Name="CareIdNo" Type="Int32" />
            <asp:Parameter Name="InvItemIdNo" Type="Int32" />
            <asp:Parameter Name="Quantity"/>
            <asp:Parameter Name="ProductIdNo" Type="Int32" />
            <asp:Parameter Name="ProductDesc" Type="String" />
            <asp:Parameter Name="UnitCost" />
            <asp:Parameter Name="UnitMeasQtyCannabis" />
            <asp:Parameter Name="AccountIPAddress" Type="String" />
            <asp:Parameter Name="AccountOrigin" Type="String" />
           </InsertParameters>

        <SelectParameters>
            <asp:Parameter Name="SessionId" Type="String" />
            <asp:Parameter Name="CustIdNo" Type="Int32" />
            <asp:Parameter Name="ProductIdNo" Type="String" />
        </SelectParameters>

        <UpdateParameters>
            <asp:Parameter Name="SessionId" Type="String" />
            <asp:Parameter Name="CustIdNo" Type="Int32" />
            <asp:Parameter Name="ProductIdNo" Type="String" />
            <asp:Parameter Name="Quantity"/>
            <asp:Parameter Name="UnitMeasQtyCannabis" />
         </UpdateParameters>
        
    </asp:SqlDataSource>
        
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>