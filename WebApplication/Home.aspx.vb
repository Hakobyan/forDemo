﻿Imports System.Data
Imports System.Configuration
Imports System.Configuration.ConfigurationSettings
Imports System.Data.SqlClient

Public Class Home
    Inherits System.Web.UI.Page
    Private db1DFConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("DFKiosk.My.MySettings.db1DFConnectionString").ConnectionString)
    Private db1DFKioskConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("DFKiosk.My.MySettings.db1DFKioskConnectionString").ConnectionString)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("SessionID") = HttpContext.Current.Session.SessionID
        Session("CartCount") = HttpContext.Current.Session("CartCount")
        Session("CannabisLimit") = HttpContext.Current.Session("CannabisLimit")
        Session("CannabisCount") = HttpContext.Current.Session("CannabisCount")

        If Not Page.IsPostBack Then
            Customer_Values()
        End If

        Dim cCustIdNo As Label
        cCustIdNo = DirectCast(Page.Master.FindControl("lblCustIdNo"), Label)

        If cCustIdNo.Text = "0" Then
            Session("CartCount") = "0"
            Session("CannabisLimit") = "0"
            Session("CannabisCount") = "0"
            Session("CannabisCanBuy") = "0"
        End If

        Dim cCartCount As Label, cCannabisCount As Label, cCannabisCanBuy As Label

        cCartCount = DirectCast(Page.Master.FindControl("lblCartCount"), Label)
        cCannabisCount = DirectCast(Page.Master.FindControl("lblCannabisCount"), Label)
        cCannabisCanBuy = DirectCast(Page.Master.FindControl("lblCannabisCanBuy"), Label)

        cCartCount.Text = Session("CartCount")
        cCannabisCount.Text = Session("CannabisCount")
        cCannabisCanBuy.Text = CStr(CDbl(Session("CannabisLimit")) - CDbl(Session("CannabisCount")))

        If Len(Request.QueryString("SRN")) = 9 Then
            Dim pnl As UpdatePanel
            pnl = DirectCast(Page.Master.FindControl("UpdatePanelCustomer"), UpdatePanel)
            pnl.Visible = False
        End If

    End Sub
    Protected Sub Customer_Values()
        If Len(Request.QueryString("SRN")) <> 9 Then
            Exit Sub
        End If

        Dim cSRNNO As String = Request.QueryString("SRN")

        'Dim adapter As New SqlDataAdapter()
        Dim Sql As String = "", cmd As SqlCommand, rdr As SqlDataReader
        'Dim ds As New DataSet()
        Dim isCust As Boolean = False

        Sql = "SELECT mail_first, id_no FROM ppcust WHERE ppcust.srn_no = '" + cSRNNO + "'"
        Try
            db1DFConn.Open()
            
            cmd = New SqlCommand(Sql, db1DFConn)
            cmd.CommandTimeout = 120
            rdr = cmd.ExecuteReader

            rdr.Read()
            If rdr.HasRows Then
                Session("SRNNO") = cSRNNO
                Session("CustIdNo") = CStr(rdr(1))
                Session("CareIdNo") = CStr(0)
                Session("CannabisLimit") = Check_Cust_Limit(CInt(rdr(1))).ToString("N3")
                
                Label3.Visible = True
                Label4.Visible = True
                Label3.Text = "HELLO " + CStr(rdr(0)) + "! You may purchase up to " + Session("CannabisLimit") + " grams of cannabis today."
                Label4.Text = "Your most recent purchases are listed below. To reorder, please enter a quantity and click on the 'Buy' button."
                isCust = True
            End If
            rdr.Close()
            
            If isCust = False Then
                Sql = "SELECT ppcare.mail_first, ppcare.id_no, ppcust.id_no Cust_Id_No "
                Sql = Sql & "FROM ppcare "
                Sql = Sql & "INNER JOIN ppcust ON ppcust.care_id_no = ppcare.id_no "
                Sql = Sql & "WHERE ppcare.srn_no = '" + cSRNNO + "'"

                cmd = New SqlCommand(Sql, db1DFConn)
                cmd.CommandTimeout = 120
                rdr = cmd.ExecuteReader
                rdr.Read()
                If rdr.HasRows Then
                    Session("SRNNO") = cSRNNO
                    Session("CustIdNo") = CInt(rdr(2))
                    Session("CareIdNo") = CInt(rdr(1))
                    Session("CannabisLimit") = Check_Cust_Limit(CInt(rdr(2))).ToString("N3")

                    Label3.Visible = True
                    Label4.Visible = True
                    Label3.Text = "HELLO " + CStr(rdr(0)) + "! You may purchase up to " + Session("CannabisLimit") + " grams of cannabis today."
                    Label4.Text = "Your most recent purchases are listed below. To reorder, please enter a quantity and click on the Buy button."
                    isCust = True
                End If
                rdr.Close()
            End If

        Catch ex As Exception
            db1DFConn.Close()
        End Try
        db1DFConn.Close()

        Dim cCustIdNo As Label, cCareIdNo As Label, cCannabisLimit As Label

        cCustIdNo = DirectCast(Page.Master.FindControl("lblCustIdNo"), Label)
        cCareIdNo = DirectCast(Page.Master.FindControl("lblCareIdNo"), Label)
        cCannabisLimit = DirectCast(Page.Master.FindControl("lblCannabisLimit"), Label)
        
        cCustIdNo.Text = Session("CustIdNo")
        cCareIdNo.Text = Session("CareIdNo")
        cCannabisLimit.Text = Session("CannabisLimit")

    End Sub
    Sub GridView_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles GridView.RowCommand
        If e.CommandName = "Buy" Then
            Dim strMessage As String, cCannabisCount As Double = 0

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = GridView.Rows(index)

            Dim Quantity As TextBox = CType(selectedRow.Cells(0).FindControl("Quantity"), TextBox)
            Dim InvItemIdNo As TableCell = selectedRow.Cells(4)
            Dim UnitCost As TableCell = selectedRow.Cells(6)
            Dim ProductIdNo As TableCell = selectedRow.Cells(7)
            Dim ProductDesc As TableCell = selectedRow.Cells(2)
            Dim UnitMeasQtyCannabis As TableCell = selectedRow.Cells(8)

            Dim dv As System.Data.DataView
            Dim myCount As Integer

            'Check Cannabis Limit
            cCannabisCount = CDbl(CDbl(Quantity.Text) * CDbl(UnitMeasQtyCannabis.Text))
            If cCannabisCount > 0 Then
                Dim cCannabisCanBuy As Label
                cCannabisCanBuy = DirectCast(Page.Master.FindControl("lblCannabisCanBuy"), Label)
                cCannabisCanBuy.Text = CStr(CDbl(Session("CannabisLimit")) - CDbl(Session("CannabisCount")))
                If cCannabisCount > cCannabisCanBuy.Text Then
                    strMessage = "This purchase of cannabis is over your limit. Please adjust the quantity."
                    TryCast(Me.Master, DF_Kiosk).CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If

            If Quantity.Text >= 1 Then
                SqlDataSource2.SelectParameters("SessionId").DefaultValue = HttpContext.Current.Session.SessionID
                SqlDataSource2.SelectParameters("CustIdNo").DefaultValue = Session("CustIdNo")
                SqlDataSource2.SelectParameters("ProductIdNo").DefaultValue = ProductIdNo.Text
                dv = CType(SqlDataSource2.Select(DataSourceSelectArguments.Empty), System.Data.DataView)
                myCount = CType(dv.Table.Rows(0)(0), Integer)
                If (myCount > 0) Then
                    SqlDataSource2.UpdateParameters("SessionId").DefaultValue = HttpContext.Current.Session.SessionID
                    SqlDataSource2.UpdateParameters("CustIdNo").DefaultValue = Session("CustIdNo")
                    SqlDataSource2.UpdateParameters("ProductIdNo").DefaultValue = ProductIdNo.Text
                    SqlDataSource2.UpdateParameters("Quantity").DefaultValue = Quantity.Text
                    SqlDataSource2.UpdateParameters("UnitMeasQtyCannabis").DefaultValue = Str(UnitMeasQtyCannabis.Text)
                    SqlDataSource2.Update()

                    TryCast(Me.Master, DF_Kiosk).Update_CartCount("Add", Quantity.Text, cCannabisCount)
                Else
                    SqlDataSource2.InsertParameters("SessionId").DefaultValue = HttpContext.Current.Session.SessionID
                    SqlDataSource2.InsertParameters("CustIdNo").DefaultValue = Session("CustIdNo")
                    SqlDataSource2.InsertParameters("CareIdNo").DefaultValue = Session("CareIdNo")
                    SqlDataSource2.InsertParameters("InvItemIdNo").DefaultValue = InvItemIdNo.Text
                    SqlDataSource2.InsertParameters("Quantity").DefaultValue = Quantity.Text
                    SqlDataSource2.InsertParameters("ProductIdNo").DefaultValue = ProductIdNo.Text
                    SqlDataSource2.InsertParameters("ProductDesc").DefaultValue = ProductDesc.Text
                    SqlDataSource2.InsertParameters("UnitCost").DefaultValue = Str(UnitCost.Text)
                    SqlDataSource2.InsertParameters("UnitMeasQtyCannabis").DefaultValue = Str(UnitMeasQtyCannabis.Text)
                    SqlDataSource2.InsertParameters("AccountIPAddress").DefaultValue = Left(Request.ServerVariables("REMOTE_ADDR"), 48)
                    SqlDataSource2.InsertParameters("AccountOrigin").DefaultValue = "DFKiosk"

                    SqlDataSource2.Insert()

                    TryCast(Me.Master, DF_Kiosk).Update_CartCount("Add", Quantity.Text, cCannabisCount)

                End If

                strMessage = "You just added " & ProductDesc.Text & ", quantity of " & Quantity.Text & " , to your cart."
                TryCast(Me.Master, DF_Kiosk).CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                strMessage = "You must enter a quantity greater than 0."
                TryCast(Me.Master, DF_Kiosk).CreateMessageAlert(Me, strMessage, "strKey1")
                selectedRow.Cells(0).Focus()
            End If
        End If
    End Sub
    Public Function Check_Cust_Limit(ByVal CustIdNo As Integer) As Double
        Dim Sql As String = "", cmd As SqlCommand, rdr As SqlDataReader
        Dim StateQuantity As Double = 0

        Check_Cust_Limit = 0
        
        Sql = "SELECT COALESCE(sum(ppbill_item.Quantity*ppinvt.unit_meas_qty_cannabis), 0) AS limitTot, "

        Sql = Sql & "(SELECT TOP 1 cannabis_max FROM ppcust WHERE Id_No = " & CustIdNo & ") AS CannabisMax, "
        Sql = Sql & "(SELECT TOP 1 State_Quantity FROM ppsettings) AS StateQuantity "

        Sql = Sql & "FROM PPBILL_ITEM "
        Sql = Sql & "INNER JOIN ppbill ON ppbill.id_no = ppbill_item.invoice_id_no "
        Sql = Sql & "INNER JOIN ppinvt ON ppinvt.id_no = ppbill_item.product_id_no "
        Sql = Sql & "WHERE ppbill.cust_id_no = " & CustIdNo & " AND "
        Sql = Sql & "ppinvt.is_cannabis = 1 AND "
        Sql = Sql & "ppbill_item.is_del = 0 AND "
        Sql = Sql & "DATEADD(day, DATEDIFF(day, (SELECT TOP 1 (State_Timeframe*-1) FROM ppsettings), ppbill.enter_date), 0) >= getdate() "
        Sql = Sql & "GROUP BY ppbill.cust_id_no"

        Try
            cmd = New SqlCommand(Sql, db1DFConn)
            cmd.CommandTimeout = 120
            rdr = cmd.ExecuteReader

            rdr.Read()
            If rdr.HasRows Then
                If CDbl(CheckNull(rdr("CannabisMax"))) > CDbl(CheckNull(rdr("StateQuantity"))) Then
                    StateQuantity = CDbl(rdr("CannabisMax"))
                Else
                    StateQuantity = CDbl(rdr("StateQuantity"))
                End If

                Check_Cust_Limit = CDbl(CDbl(StateQuantity) - CDbl(rdr("limitTot")))
            End If
            rdr.Close()
        Catch ex As SqlException
            MsgBox("Error:" & ex.ToString)
        End Try

    End Function
    Protected Function CheckNull(ByVal o As Object) As String
        If IsDBNull(o) Then
            Return CStr(0)
        Else
            Return CStr(o)
        End If
    End Function

End Class