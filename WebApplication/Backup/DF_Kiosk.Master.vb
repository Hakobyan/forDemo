﻿Imports System.Data
Imports System.Configuration
Imports System.Configuration.ConfigurationSettings
Imports System.Data.SqlClient
Public Class DF_Kiosk
    Inherits System.Web.UI.MasterPage
    Private db1DFConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("DFKiosk.My.MySettings.db1DFConnectionString").ConnectionString)
    Private db1DFKioskConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("DFKiosk.My.MySettings.db1DFKioskConnectionString").ConnectionString)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtSRNNo.Focus()
    End Sub
    Protected Sub txtSRNNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSRNNo.TextChanged
        Dim isHomePage As Boolean = True

        If Len(txtSRNNo.Text) = 9 Then
            Response.Redirect("Home.aspx?SRN=" & txtSRNNo.Text & "")
        End If

    End Sub

    Protected Sub btnImageCheckout_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnImageCheckout.Click
        Response.Redirect("Cart.aspx")
    End Sub
    Protected Sub btnLogout_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        Session("SessionID") = ""
        Session("SRNNO") = ""
        Session("CustIdNo") = "0"
        Session("CareIdNo") = "0"
        Session("CannabisLimit") = "0"
        Session("CartCount") = "0"
        Session("CannabisCount") = "0"

        Response.Redirect("Home.aspx")
    End Sub
    Protected Sub btnViewInventory_Click(sender As Object, e As EventArgs) Handles btnViewInventory.Click
        Dim strmessage As String = "Please swipe your registration card!"
        
        If lblCustIdNo.Text = "0" Then 'this is the Home Page
            If Len(Request.QueryString("SRN")) <> 9 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "myAlert", "alert('" & strmessage & "');", True)
                Exit Sub
            End If
        End If

        Response.Redirect("Inventory.aspx")
    End Sub
    Protected Sub btnConsult_Click(sender As Object, e As EventArgs) Handles btnConsult.Click
        Dim strmessage As String = "Please swipe your registration card!"
        Dim NumberInQuene As Integer = 0
        Dim CustIdNo As Integer = 0, CareIdNo As Integer = 0, Company As String = "", CompanyCare As String = ""

        If lblCustIdNo.Text = "0" Then 'this is the Home Page
            If Len(Request.QueryString("SRN")) <> 9 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "myAlert", "alert('" & strmessage & "');", True)
                Exit Sub
            Else
                CustIdNo = CInt(lblCustIdNo.Text)
                CareIdNo = CInt(lblCareIdNo.Text)
            End If
        Else
           CustIdNo = CInt(lblCustIdNo.Text)
            CareIdNo = CInt(lblCareIdNo.Text)
        End If

        Dim Sql As String = "", cmd As SqlCommand, rdr As SqlDataReader
        
        Sql = "SELECT ppcust.company, ppcare.company company_care FROM ppcust "
        Sql = Sql & "LEFT JOIN ppcare ON ppcare.id_no = ppcust.care_id_no "
        Sql = Sql & "WHERE ppcust.id_no = " & CustIdNo
        Try
            db1DFConn.Open()
            cmd = New SqlCommand(Sql, db1DFConn)
            cmd.CommandTimeout = 120
            rdr = cmd.ExecuteReader
            rdr.Read()
            If rdr.HasRows Then
                Company = CStr(rdr(0))
                If CareIdNo > 0 Then
                    CompanyCare = CStr(rdr(1))
                End If
            End If
            rdr.Close()
        Catch ex As Exception
            db1DFConn.Close()
        End Try
        db1DFConn.Close()

        SqlDataSource1.InsertParameters("SessionId").DefaultValue = HttpContext.Current.Session.SessionID
        SqlDataSource1.InsertParameters("CustIdNo").DefaultValue = CustIdNo
        SqlDataSource1.InsertParameters("CareIdNo").DefaultValue = CareIdNo
        SqlDataSource1.InsertParameters("Company").DefaultValue = Company
        SqlDataSource1.InsertParameters("Company_Care").DefaultValue = CompanyCare
        SqlDataSource1.InsertParameters("AccountIPAddress").DefaultValue = Left(Request.ServerVariables("REMOTE_ADDR"), 48)
        SqlDataSource1.InsertParameters("AccountOrigin").DefaultValue = "DFKiosk"
        SqlDataSource1.Insert()

        Sql = "SELECT COUNT(ID_NO) FROM consult WHERE Completed = 0"
        Try
            db1DFKioskConn.Open()

            cmd = New SqlCommand(Sql, db1DFKioskConn)
            cmd.CommandTimeout = 120
            Dim countTot As Int32 = Convert.ToInt32(cmd.ExecuteScalar())
            If countTot > 0 Then
                NumberInQuene = countTot - 1
            End If

            Sql = "SELECT cust_id_no, Id_No FROM consult "
            Sql = Sql & "WHERE (SessionID = '" + HttpContext.Current.Session.SessionID + "') AND "
            Sql = Sql & "(cust_Id_No = " & CustIdNo & ")"

            cmd = New SqlCommand(Sql, db1DFKioskConn)
            cmd.CommandTimeout = 120
            rdr = cmd.ExecuteReader
            rdr.Read()
            If rdr.HasRows Then
                strmessage = "We look forward to meeting with you! "
                strmessage = strmessage & "Your quene # is " + CStr(rdr(0)) + "! "
                strmessage = strmessage & "There are " + CStr(NumberInQuene) + " patients in front of you!"
            Else
                strmessage = "There was a problem scheduling your consultation! Please try again."
            End If
            rdr.Close()

        Catch ex As Exception
            db1DFKioskConn.Close()
        End Try
        db1DFKioskConn.Close()

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "myAlert", "alert('" & strmessage & "');", True)

    End Sub
    Public Sub CreateMessageAlert(ByRef aspxPage As System.Web.UI.Page, ByVal strMessage As String, ByVal strKey As String)
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(),
        "myAlert", "alert('" & strMessage & "');", True)
    End Sub
    Public Sub CreateMessageAlert_End(ByRef aspxPage As System.Web.UI.Page, ByVal strMessage As String, ByVal strKey As String)
        Session("SessionID") = ""
        Session("SRNNO") = ""
        Session("CustIdNo") = "0"
        Session("CareIdNo") = "0"
        Session("CannabisLimit") = "0"
        Session("CartCount") = "0"
        Session("CannabisCount") = "0"

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(),
        "myAlert", "alert('" & strMessage & "');window.location='home.aspx';", True)
    End Sub
    Public Sub Update_CartCount(cThisType As String, Quantity As Double, QuantityCannabis As Double)
        If cThisType = "Add" Then
            Session("CartCount") = CStr(CDbl(Session("CartCount")) + CDbl(Quantity))
            Session("CannabisCount") = CStr(CDbl(Session("CannabisCount")) + CDbl(QuantityCannabis))
        ElseIf cThisType = "Subtract" Then
            Session("CartCount") = CStr(CDbl(Session("CartCount")) - CDbl(Quantity))
            Session("CannabisCount") = CStr(CDbl(Session("CannabisCount")) - CDbl(QuantityCannabis))
        Else
            Session("CartCount") = CDbl(Quantity)
            Session("CannabisCount") = CDbl(QuantityCannabis)
        End If

        lblCartCount.Text = Session("CartCount")
        lblCannabisCount.Text = Session("CannabisCount")
        lblCannabisCanBuy.Text = CStr(CDbl(lblCannabisLimit.Text) - CDbl(Session("CannabisCount")))
    End Sub
End Class