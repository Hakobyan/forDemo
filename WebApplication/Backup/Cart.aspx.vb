﻿Imports System.Data
Imports System.Configuration
Imports System.Configuration.ConfigurationSettings
Imports System.Data.SqlClient
Public Class Cart
    Inherits System.Web.UI.Page
    Private db1DFKioskConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("DFKiosk.My.MySettings.db1DFKioskConnectionString").ConnectionString)

    Dim GrandTotal As Double = 0
    Dim GrandTotalGrams As Double = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("SessionID") = HttpContext.Current.Session.SessionID
        Session("SRNNO") = HttpContext.Current.Session("SRNNO")
        Session("CustIdNo") = HttpContext.Current.Session("CustIdNo")
        Session("CareIdNo") = HttpContext.Current.Session("CareIdNo")
        Session("CannabisLimit") = HttpContext.Current.Session("CannabisLimit")
        Session("CartCount") = HttpContext.Current.Session("CartCount")
        Session("CannabisCount") = HttpContext.Current.Session("CannabisCount")

        Dim cCustIdNo As Label, cCareIdNo As Label, cCannabisLimit As Label
        Dim cCannabisCount As Label, cCartCount As Label, cCannabisCanBuy As Label

        cCustIdNo = DirectCast(Page.Master.FindControl("lblCustIdNo"), Label)
        cCareIdNo = DirectCast(Page.Master.FindControl("lblCareIdNo"), Label)
        cCannabisLimit = DirectCast(Page.Master.FindControl("lblCannabisLimit"), Label)
        cCartCount = DirectCast(Page.Master.FindControl("lblCartCount"), Label)
        cCannabisCount = DirectCast(Page.Master.FindControl("lblCannabisCount"), Label)
        cCannabisCanBuy = DirectCast(Page.Master.FindControl("lblCannabisCanBuy"), Label)
        
        cCustIdNo.Text = Session("CustIdNo")
        cCareIdNo.Text = Session("CareIdNo")
        cCannabisLimit.Text = Session("CannabisLimit")
        cCartCount.Text = Session("CartCount")
        cCannabisCount.Text = Session("CannabisCount")
        cCannabisCanBuy.Text = CStr(CDbl(Session("CannabisLimit")) - CDbl(Session("CannabisCount")))

        Dim pnl As UpdatePanel
        pnl = DirectCast(Page.Master.FindControl("UpdatePanelCustomer"), UpdatePanel)
        pnl.Visible = False

        If Not Page.IsPostBack Then
            SqlDataSource2.SelectParameters("SessionId").DefaultValue = HttpContext.Current.Session.SessionID
            SqlDataSource2.SelectParameters("CustIdNo").DefaultValue = Session("CustIdNo")
        End If
    End Sub
    Protected Sub GridView_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles GridView.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim strMessage As String

        Dim selectedRow As GridViewRow = GridView.Rows(index)

        Dim Quantity As TextBox = CType(selectedRow.Cells(2).FindControl("Quantity"), TextBox)
        Dim IdNo As TableCell = selectedRow.Cells(9)
        Dim ProductDesc As TableCell = selectedRow.Cells(1)
        Dim UnitMeasQtyCannabis As TableCell = selectedRow.Cells(7)

        If e.CommandName = "UpdateItem" Then
            SqlDataSource2.UpdateParameters("IdNo").DefaultValue = IdNo.Text
            SqlDataSource2.UpdateParameters("Quantity").DefaultValue = Quantity.Text
            SqlDataSource2.UpdateParameters("UnitMeasQtyCannabis").DefaultValue = Str(UnitMeasQtyCannabis.Text)
            SqlDataSource2.Update()

            GridView.DataBind()

            strMessage = "You just updated " & ProductDesc.Text & ", quantity of " & Quantity.Text & "."
            TryCast(Me.Master, DF_Kiosk).CreateMessageAlert(Me, strMessage, "strKey1")

            Update_CartCount_SQL()

        ElseIf e.CommandName = "DeleteItem" Then
            SqlDataSource2.DeleteParameters("IdNo").DefaultValue = IdNo.Text
            SqlDataSource2.Delete()

            GridView.DataBind()

            strMessage = "You just deleted " & ProductDesc.Text & ", quantity of " & Quantity.Text & "."
            TryCast(Me.Master, DF_Kiosk).CreateMessageAlert(Me, strMessage, "strKey1")

            Update_CartCount_SQL()
        End If
    End Sub
    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles GridView.RowDataBound
        Select Case e.Row.RowType
            Case Is = DataControlRowType.DataRow
                GrandTotal = GrandTotal + Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Total"))
                GrandTotalGrams = GrandTotalGrams + Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TotalGrams"))

            Case Is = DataControlRowType.Footer
                e.Row.Cells(4).Text = "Order Total:"
                e.Row.Cells(5).Text = GrandTotal.ToString("c")

        End Select

    End Sub
    Protected Sub btnPlaceOrder_Click(sender As Object, e As EventArgs) Handles btnPlaceOrder.Click
        SqlDataSource3.UpdateParameters("SessionId").DefaultValue = HttpContext.Current.Session.SessionID
        SqlDataSource3.Update()

        Dim Sql As String = "", cmd As SqlCommand, rdr As SqlDataReader
        Dim strmessage As String = "", NumberInQuene As Integer = 0

        Sql = "SELECT COUNT(ID_NO) FROM cart WHERE Completed = 1 AND Exported = 0"
        Try
            db1DFKioskConn.Open()

            cmd = New SqlCommand(Sql, db1DFKioskConn)
            cmd.CommandTimeout = 120
            Dim countTot As Int32 = Convert.ToInt32(cmd.ExecuteScalar())
            If countTot > 0 Then
                NumberInQuene = countTot - 1
            End If

            Sql = "SELECT cust_id_no, Id_No FROM cart WHERE SessionID = '" + HttpContext.Current.Session.SessionID + "'"
            cmd = New SqlCommand(Sql, db1DFKioskConn)
            cmd.CommandTimeout = 120
            rdr = cmd.ExecuteReader
            rdr.Read()
            If rdr.HasRows Then
                strmessage = "We thank you for your order! "
                strmessage = strmessage & "Your quene # is " + CStr(rdr(0)) + "! "
                strmessage = strmessage & "There are " + CStr(NumberInQuene) + " patients in front of you!"
            End If
            rdr.Close()

        Catch ex As Exception
            db1DFKioskConn.Close()
        End Try
        db1DFKioskConn.Close()

        TryCast(Me.Master, DF_Kiosk).CreateMessageAlert_End(Me, strmessage, "strKey1")

    End Sub

    Protected Sub btnCancelOrder_Click(sender As Object, e As EventArgs) Handles btnCancelOrder.Click
        Dim strmessage As String = ""

        SqlDataSource3.DeleteParameters("SessionId").DefaultValue = HttpContext.Current.Session.SessionID
        SqlDataSource3.Delete()

        strmessage = "You order has been cancelled."

        TryCast(Me.Master, DF_Kiosk).CreateMessageAlert_End(Me, strmessage, "strKey1")
    End Sub
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("Home.aspx?SRN=" & Session("SRNNO") & "")
    End Sub
    Protected Sub Update_CartCount_SQL()
        Dim Sql As String = "", cmd As SqlCommand, rdr As SqlDataReader

        Sql = "SELECT SUM(Quantity) AS Quantity, SUM(Unit_Meas_Qty_Cannabis*Quantity) AS Grams "
        Sql = Sql & "FROM cart WHERE Cust_Id_No = " & Session("CustIdNo") & " AND "
        Sql = Sql & "SessionID = '" + Session("SessionID") + "'"

        Try
            db1DFKioskConn.Open()

            cmd = New SqlCommand(Sql, db1DFKioskConn)
            cmd.CommandTimeout = 120
            rdr = cmd.ExecuteReader
            rdr.Read()
            If rdr.HasRows Then
                TryCast(Me.Master, DF_Kiosk).Update_CartCount("Equals", rdr(0), rdr(1))
            End If
            rdr.Close()

        Catch ex As Exception
            db1DFKioskConn.Close()
        End Try
        db1DFKioskConn.Close()

    End Sub
End Class