﻿Imports System.Data
Imports System.Configuration
Imports System.Configuration.ConfigurationSettings
Imports System.Data.SqlClient
Public Class Inventory
    Inherits System.Web.UI.Page
    Private db1DFConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("DFKiosk.My.MySettings.db1DFConnectionString").ConnectionString)
    Private db1DFKioskConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("DFKiosk.My.MySettings.db1DFKioskConnectionString").ConnectionString)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("SessionID") = HttpContext.Current.Session.SessionID
        Session("SRNNO") = HttpContext.Current.Session("SRNNO")
        Session("CustIdNo") = HttpContext.Current.Session("CustIdNo")
        Session("CareIdNo") = HttpContext.Current.Session("CareIdNo")
        Session("CannabisLimit") = HttpContext.Current.Session("CannabisLimit")
        Session("CartCount") = HttpContext.Current.Session("CartCount")
        Session("CannabisCount") = HttpContext.Current.Session("CannabisCount")

        Dim cCustIdNo As Label, cCareIdNo As Label, cCannabisLimit As Label
        Dim cCannabisCount As Label, cCartCount As Label, cCannabisCanBuy As Label

        cCustIdNo = DirectCast(Page.Master.FindControl("lblCustIdNo"), Label)
        cCareIdNo = DirectCast(Page.Master.FindControl("lblCareIdNo"), Label)
        cCannabisLimit = DirectCast(Page.Master.FindControl("lblCannabisLimit"), Label)
        cCartCount = DirectCast(Page.Master.FindControl("lblCartCount"), Label)
        cCannabisCount = DirectCast(Page.Master.FindControl("lblCannabisCount"), Label)
        cCannabisCanBuy = DirectCast(Page.Master.FindControl("lblCannabisCanBuy"), Label)

        cCustIdNo.Text = Session("CustIdNo")
        cCareIdNo.Text = Session("CareIdNo")
        cCannabisLimit.Text = Session("CannabisLimit")
        cCartCount.Text = Session("CartCount")
        cCannabisCount.Text = Session("CannabisCount")
        cCannabisCanBuy.Text = CStr(CDbl(Session("CannabisLimit")) - CDbl(Session("CannabisCount")))

        Dim pnl As UpdatePanel
        pnl = DirectCast(Page.Master.FindControl("UpdatePanelCustomer"), UpdatePanel)
        pnl.Visible = False

        If Not Page.IsPostBack Then

        End If

        panel.Visible = False
    End Sub
    Private Sub GridView_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView.PageIndexChanging
        GridView.PageIndex = e.NewPageIndex
    End Sub


    Sub GridView_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles GridView.RowCommand
        If e.CommandName = "Buy" Then
            Dim strMessage As String, cCannabisCount As Double = 0

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = GridView.Rows(index)

            Dim Quantity As TextBox = CType(selectedRow.Cells(1).FindControl("Quantity"), TextBox)
            Dim UnitCost As TableCell = selectedRow.Cells(4)
            Dim ProductIdNo As TableCell = selectedRow.Cells(8)
            Dim ProductDesc As TableCell = selectedRow.Cells(6)
            Dim VarietyImage As TableCell = selectedRow.Cells(7)
            Dim UnitMeasQtyCannabis As TableCell = selectedRow.Cells(9)

            Dim dv As System.Data.DataView
            Dim myCount As Integer

            'Check Cannabis Limit
            cCannabisCount = CDbl(CDbl(Quantity.Text) * CDbl(UnitMeasQtyCannabis.Text))
            If cCannabisCount > 0 Then
                Dim cCannabisCanBuy As Label
                cCannabisCanBuy = DirectCast(Page.Master.FindControl("lblCannabisCanBuy"), Label)
                cCannabisCanBuy.Text = CStr(CDbl(Session("CannabisLimit")) - CDbl(Session("CannabisCount")))
                If cCannabisCount > cCannabisCanBuy.Text Then
                    strMessage = "This purchase of cannabis is over your limit. Please adjust the quantity."
                    TryCast(Me.Master, DF_Kiosk).CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If

            If Quantity.Text >= 1 Then
                SqlDataSource2.SelectParameters("SessionId").DefaultValue = HttpContext.Current.Session.SessionID
                SqlDataSource2.SelectParameters("CustIdNo").DefaultValue = Session("CustIdNo")
                SqlDataSource2.SelectParameters("ProductIdNo").DefaultValue = ProductIdNo.Text
                dv = CType(SqlDataSource2.Select(DataSourceSelectArguments.Empty), System.Data.DataView)
                myCount = CType(dv.Table.Rows(0)(0), Integer)
                If (myCount > 0) Then
                    SqlDataSource2.UpdateParameters("SessionId").DefaultValue = HttpContext.Current.Session.SessionID
                    SqlDataSource2.UpdateParameters("CustIdNo").DefaultValue = Session("CustIdNo")
                    SqlDataSource2.UpdateParameters("ProductIdNo").DefaultValue = ProductIdNo.Text
                    SqlDataSource2.UpdateParameters("Quantity").DefaultValue = Quantity.Text
                    SqlDataSource2.UpdateParameters("UnitMeasQtyCannabis").DefaultValue = Str(UnitMeasQtyCannabis.Text)
                    SqlDataSource2.Update()

                    TryCast(Me.Master, DF_Kiosk).Update_CartCount("Add", Quantity.Text, cCannabisCount)
                Else
                    SqlDataSource2.InsertParameters("SessionId").DefaultValue = HttpContext.Current.Session.SessionID
                    SqlDataSource2.InsertParameters("CustIdNo").DefaultValue = Session("CustIdNo")
                    SqlDataSource2.InsertParameters("CareIdNo").DefaultValue = Session("CareIdNo")
                    SqlDataSource2.InsertParameters("InvItemIdNo").DefaultValue = 0
                    SqlDataSource2.InsertParameters("Quantity").DefaultValue = Quantity.Text
                    SqlDataSource2.InsertParameters("ProductIdNo").DefaultValue = ProductIdNo.Text
                    SqlDataSource2.InsertParameters("ProductDesc").DefaultValue = ProductDesc.Text
                    SqlDataSource2.InsertParameters("UnitCost").DefaultValue = Str(UnitCost.Text)
                    SqlDataSource2.InsertParameters("UnitMeasQtyCannabis").DefaultValue = Str(UnitMeasQtyCannabis.Text)
                    SqlDataSource2.InsertParameters("AccountIPAddress").DefaultValue = Left(Request.ServerVariables("REMOTE_ADDR"), 48)
                    SqlDataSource2.InsertParameters("AccountOrigin").DefaultValue = "DFKiosk"

                    SqlDataSource2.Insert()

                    TryCast(Me.Master, DF_Kiosk).Update_CartCount("Add", Quantity.Text, cCannabisCount)

                End If

                strMessage = "You just added " & ProductDesc.Text & ", quantity of " & Quantity.Text & " , to your cart."
                TryCast(Me.Master, DF_Kiosk).CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                strMessage = "You must enter a quantity greater than 0."
                TryCast(Me.Master, DF_Kiosk).CreateMessageAlert(Me, strMessage, "strKey1")
                selectedRow.Cells(0).Focus()
            End If
        End If
    End Sub
    Protected Sub can1_Click(sender As Object, e As EventArgs) Handles can1.Click
        panel.Visible = False
    End Sub
    Protected Sub ImageButton_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs)
        Dim btn As ImageButton = CType(sender, ImageButton)
        Dim CommandName As String = btn.CommandName
        Dim CommandArgument As String = btn.CommandArgument

        Image1.ImageUrl = CommandArgument
        panel.Visible = True
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("Home.aspx?SRN=" & Session("SRNNO") & "")
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If Len(Trim(txtSearch.Text)) = 0 Then
            txtSearchHidden.Text = "X"
            Exit Sub
        End If

        txtSearchHidden.Text = txtSearch.Text

        SqlDataSource1.SelectParameters("ProductDesc").DefaultValue = txtSearch.Text
        GridView.DataBind()

        txtSearch.Text = ""

    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCategory.SelectedIndexChanged
        txtSearchHidden.Text = "X"
    End Sub

    Protected Sub ddlStrain_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStrain.SelectedIndexChanged
        txtSearchHidden.Text = "X"
    End Sub

    Private Sub GridView_Unload(sender As Object, e As System.EventArgs) Handles GridView.Unload
        txtSearchHidden.Text = "X"
    End Sub
End Class